<footer class="page_footer ls section_padding_top_110 section_padding_bottom_130 columns_margin_bottom_40 columns_padding_30">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-lg-4 text-center text-sm-left">
							<div class="widget widget_about">
								<div class="logo bottommargin_5"> <img src="images/logo-dark.png" alt=""> </div>
								<p>Acá va un resumen del texto de quienes somos</p>
								<div class="page_social topmargin_25"> <a href="https://www.facebook.com/mvdasesores-2679543535453610/" class="social-icon bg-icon rounded-icon socicon-facebook"></a> <a href="https://www.instagram.com/mvd_asesores/" class="social-icon bg-icon rounded-icon socicon-instagram"></a>									</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-lg-4 text-center text-sm-left">
							<div class="widget widget_text">
								<h3 class="widget-title">Nuestra Oficina</h3>
								<p class="bottommargin_15">  Av. 18 de Julio 841 Of. 102, Montevideo, Uruguay<br> <a href="mailto:#">info@mvdasesores.com</a> <br> (598) 2706-4443  </p>
								<p> Lun a Vie: 9:00 - 19:00</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-lg-4 text-center text-sm-left">
							<div class="widget widget_nav_menu highlghtlinks">
								<h3 class="widget-title">Accesos rapidos</h3>
								<ul class="menu">
									<li> <a href="index.php">Inicio</a> </li>
									<li> <a href="services.php">Servicios</a> </li>
									<li> <a href="about.php">Quienes Somos</a> </li>
									<li> <a href="contact3.php">Contacto</a> </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<section class="page_copyright ds section_padding_top_15 section_padding_bottom_15">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<p>&copy; 2020 MVD Asesores. Todos los derechos reservados.</p>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- eof #box_wrapper -->
	</div>
	<!-- eof #canvas -->
	<script src="js/compressed.js"></script>
	<script src="js/selectize.min.js"></script>
	<script src="js/main.js"></script>
	<!-- Google Map Script -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTwYSMRGuTsmfl2z_zZDStYqMlKtrybxo"></script>