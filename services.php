<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php require_once("partials/head.php"); ?>

<body>
	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->
	<?php require_once("partials/header.php"); ?>
							<div class="col-xs-12 col-sm-9 col-lg-4 text-center text-sm-right page_breadcrumbs">
								<h1 class="highlight2">Servicios</h1>
								
							</div>
						</div>
					</div>
				</header>
			</div>
			<section id="services" class="ls background_cover section_padding_top_130 section_padding_bottom_120">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="isotope_container isotope row masonry-layout columns_margin_bottom_50">
								<div class="isotope-item col-xs-12 col-md-6 col-lg-3 text-center">
									<article class="teaser"> <a href="service-single.html">
                                <img src="images/service-icons/strategy.png" alt="">
                            </a>
										<div class="toppadding_5"></div>
										<h3 class="entry-title small"> <a href="service-single.html">Consultoría</a> </h3>
										<p>Personalizamos el trabajo en el área económica, financiera, de gestión y operaciones. Planificando y
										desarrollamos el trabajo equipo para llegar a una solución a medida.</p>
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-md-6 col-lg-3 text-center">
									<article class="teaser"> <a href="service-single.html">
                                <img src="images/service-icons/business.png" alt="">
                            </a>
										<div class="toppadding_5"></div>
										<h3 class="entry-title small"> <a href="service-single.html">Asesoramiento Fiscal e Impuestos</a> </h3>
										<p>Buscamos el escenario ideal para una segura toma de decisiones, con asesoramiento, liquidación y
planificación fiscal. También lograr una estructura eficiente y minimizará los costos impositivos.</p>
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-md-6 col-lg-3 text-center">
									<article class="teaser"> <a href="service-single.html">
                                <img src="images/service-icons/marketing.png" alt="">
                            </a>
										<div class="toppadding_5"></div>
										<h3 class="entry-title small"> <a href="service-single.html">Asesoramiento Contable</a> </h3>
										<p>A tu esfuerzo le sumamos nuestras herramientas y conocimiento con servicios, tanto locales como
offshore. Crecer con una contabilidad ordenada generará buenas decisiones. </p>
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-md-6 col-lg-3 text-center">
									<article class="teaser"> <a href="service-single.html">
                                <img src="images/service-icons/optimize.png" alt="">
                            </a>
										<div class="toppadding_5"></div>
										<h3 class="entry-title small"> <a href="service-single.html">Asesoramiento Laboral</a> </h3>
										<p>Mediante la constante actualización de la normativa, brindamos soluciones a medida para permitir a las
empresas cumplir con las obligaciones legales y cargas sociales aplicables a su actividad..</p>
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-md-6 col-lg-3 text-center">
									<article class="teaser"> <a href="service-single.html">
                                <img src="images/service-icons/studies.png" alt="">
                            </a>
										<div class="toppadding_5"></div>
										<h3 class="entry-title small"> <a href="service-single.html">Administración y Gestión</a> </h3>
										<p>Elaboramos informes para la gestión del negocio y nos encargamos de los trámites en organismos
públicos, gestiones necesarias para facilitar al cliente el cumplimiento de los objetivos.</p>
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-md-6 col-lg-3 text-center">
									<article class="teaser"> <a href="service-single.html">
                                <img src="images/service-icons/research.png" alt="">
                            </a>
										<div class="toppadding_5"></div>
										<h3 class="entry-title small"> <a href="service-single.html">Proyectos de Inversión</a> </h3>
										<p>Asesoramiento y elaboración de proyectos de inversión. Presentación y seguimiento de proyectos de
inversión ante COMAP.</p>
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-md-6 col-lg-3 text-center">
									<article class="teaser"> <a href="service-single.html">
                                <img src="images/service-icons/growth.png" alt="">
                            </a>
										<div class="toppadding_5"></div>
										<h3 class="entry-title small"> <a href="service-single.html">Auditoría</a> </h3>
										<p>Elaboramos informes de auditoría y revisión limitada a través de procedimientos preestablecidos,
permitiendo al cliente obtener seguridad en la información proporcionada.</p>
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-md-6 col-lg-3 text-center">
									<article class="teaser"> <a href="service-single.html">
                                <img src="images/service-icons/growth.png" alt="">
                            </a>
										<div class="toppadding_5"></div>
										<h3 class="entry-title small"> <a href="service-single.html">Evaluación del Negocio</a> </h3>
										<p>Realizamos análisis de la inversión y rentabilidad de nuevos negocios o la incorporación de nuevos
productos.</p>
									</article>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<?php require_once("partials/footer.php"); ?>
	<!--  -->
</body>

</html>