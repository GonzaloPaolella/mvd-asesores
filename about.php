<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php require_once("partials/head.php"); ?>

<body>
	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->
			<?php require_once("partials/header.php"); ?>
							<div class="col-xs-12 col-sm-9 col-lg-4 text-center text-sm-right page_breadcrumbs">
								<h1 class="highlight2">¿Quienes somos?</h1>
								
							</div>
						</div>
					</div>
				</header>
			</div>
			<section id="about" class="ls ms background_cover section_padding_top_130 section_padding_bottom_130">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-7 col-lg-6 text-center text-sm-left"> <span class="above_heading highlight">bienvenidos a MVD Asesores</span>
							<h2 class="section_header">Sobre Nosotros</h2>
							<p class="section_excerpt grey">To meet <span class="highlight2">today's challenges</span>, we've created a unique, more personal approach to Financial Services solutions.</p>
							<p class="fontsize_18">With a community of over 400 million users (and a majority age group being 18 to 29), wouldn’t you want to make sure you’re monitoring what people are posting on social networks? Especially if the demographic of your audience falls in that age
								range.</p>
							<p class="fontsize_18">Etiam ante elit, suscipit ut enim id, vehicula vehicula odio. Suspendisse egestas accumsan leo. Vivamus facilisis, velit sit amet lobortis congue, dui dolor maximus enim, et placerat risus turpis id neque. Proin eget diam leo. Sed a felis sit amet
								neque ornare sollicitudin. </p>
						</div>
					</div>
				</div>
			</section>
			<section id="team" class="ds section_padding_top_90 section_padding_bottom_90">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header">Nuestro Equipo</h2>
							<p class="grey">Cada uno de nuestros miembros esta comprometido en lograr el mejor resultado para nuestros clientes. Conoce algunos de los expertos que trabajaran en tu caso:</p>
						</div>
					</div>
				</div>
			</section>
			<section class="ls section_padding_bottom_100 fluid_padding_0 columns_padding_0 columns_margin_0" style="border-bottom: 1px solid lightgrey">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12 text-center">
							<div class="owl-carousel owl-center-scale" data-dots="true" data-nav="false" data-loop="true" data-center="true" data-margin="0" data-responsive-xlg="3" data-responsive-lg="3" data-responisve-md="3" data-responisve-sm="3" data-responsive-xs="2">
								<article class="vertical-item">
									<div class="item-media"> <img src="images/team/image2.jpeg" alt=""> </div>
									<div class="item-content">
										<header class="entry-header"> <span class="small-text highlight">ceo / founder</span>
											<h3 class="entry-title"> Cra. Maira Peña Rey</h3>
											<p>Email: maira.pena@mvdasesores.com</p>
											<p>Cel: +598 98 81 20 28</p>
											<p><a href="https://www.linkedin.com/in/maira-pe%C3%B1a-rey/" class="social-icon bg-icon rounded-icon socicon-linkedin"></a></p>
										</header>
									</div>
								</article>
								<article class="vertical-item">
									<div class="item-media"> <img src="images/team/image1.jpeg" alt=""></div>
									<div class="item-content">
										<header class="entry-header"> <span class="small-text highlight">Financial expert</span>
											<h3 class="entry-title">Cr. José Luis Galán</h3>
											<p>Email: jose.galan@mvdasesores.com</p>
											<p>Cel: +598 92 81 20 29</p>
											<p><a href=" https://www.linkedin.com/in/jose-luis-galan-varela-00729885
" class="social-icon bg-icon rounded-icon socicon-linkedin"></a></p>
										</header>
									</div>
								</article>
								<article class="vertical-item">
									<div class="item-media"> <img src="images/team/image0.jpeg" alt="">  </div>
									<div class="item-content">
										<header class="entry-header"> <span class="small-text highlight">Compiance specialist</span>
											<h3 class="entry-title"> Cra. Romina Peña Rey</h3>
											<p>Email:  romina.pena@mvdasesores.com</p>
											<p>Cel: +598 92 81 20 27</p>
											<p><a href="https://www.linkedin.com/in/romina-pe%C3%B1a-52378775" class="social-icon bg-icon rounded-icon socicon-linkedin"></a></p>
										</header>
									</div>
								</article>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- <section id="testimonials" class="ls ms background_cover section_padding_top_130 section_padding_bottom_125">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-6 col-md-offset-6 text-center text-md-left">
							<h2 class="section_header">Opiniones de Clientes</h2>
							<hr class="divider_60_2">
							<div class="owl-carousel testimonials-owl-carousel" data-responsive-lg="1" data-responsive-md="1" data-responsive-sm="1" data-dots="true" data-nav="false">
								<div>
									<div class="star-rating" title="Rated 4.00 out of 5"> <span style="width:80%">
								<strong class="rating">4.00</strong> out of 5
							</span> </div>
									<blockquote>
										<p>"I had an overall amazing experience at RCA's Lighthouse location. The building and physical amenities were well beyond.</p>
										<footer class="media inline-block">
											<div class="media-left media-middle"> <img src="images/faces/01.jpg" alt=""> </div>
											<div class="media-body media-middle"> <cite>
										Anna Of Sewell, NJ
										<span class="highlight">client</span>
									</cite> </div>
										</footer>
									</blockquote>
								</div>
								<div>
									<div class="star-rating" title="Rated 9.00 out of 5"> <span style="width:100%">
								<strong class="rating">5.00</strong> out of 5
							</span> </div>
									<blockquote>
										<p>Maecenas sit amet lorem tristique, rutrum nibh at, venenatis elit. Orci varius natoque penatibus et magnis dis parturient montes.</p>
										<footer class="media inline-block">
											<div class="media-left media-middle"> <img src="images/faces/02.jpg" alt=""> </div>
											<div class="media-body media-middle"> <cite>
										Jerry Mayville, NJ
										<span class="highlight">client</span>
									</cite> </div>
										</footer>
									</blockquote>
								</div>
								<div>
									<div class="star-rating" title="Rated 4.50 out of 5"> <span style="width:90%">
								<strong class="rating">4.50</strong> out of 5
							</span> </div>
									<blockquote>
										<p>Sed hendrerit erat massa, ac laoreet felis posuere non. Nunc laoreet, augue sed pharetra rutrum, justo ipsum porttitor velit, quis mattis dolor quam vitae nulla.</p>
										<footer class="media inline-block">
											<div class="media-left media-middle"> <img src="images/faces/03.jpg" alt=""> </div>
											<div class="media-body media-middle"> <cite>
										Julia Baudoin, NJ
										<span class="highlight">client</span>
									</cite> </div>
										</footer>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="footer-subscribe" class="ds section_padding_top_90 section_padding_bottom_90">
				<div class="container">
					<div class="row flex-wrap v-center">
						<div class="col-xs-12 col-md-6 text-center text-md-left">
							<h2 class="section_header">Stay In Touch</h2>
						</div>
						<div class="col-xs-12 col-md-6 text-center">
							<div class="widget widget_mailchimp">
								<form class="signup" action="./" method="get">
									<div class="form-group"> <input class="mailchimp_email form-control" name="email" type="email" placeholder="Email Address"> <button type="submit" class="theme_button">Suscribirme</button> </div>
									<div class="response"></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section> -->
			<?php require_once("partials/footer.php"); ?>
	<!--  -->
</body>

</html>