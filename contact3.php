<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php require_once("partials/head.php"); ?>

<body>
	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->
	<?php require_once("partials/header.php"); ?>
							<div class="col-xs-12 col-sm-9 col-lg-4 text-center text-sm-right page_breadcrumbs">
								<h1 class="highlight2">Contacto</h1>
								
							</div>
						</div>
					</div>
				</header>
			</div>
			<section class="ls section_padding_top_130 section_padding_bottom_130">
				<div class="container">
					<div class="row">
						<div class="col-sm-4 to_animate" data-animation="pullDown">
							<div class="teaser text-center">
								<div class="teaser_icon highlight size_small"> <i class="rt-icon2-phone5"></i> </div>
								<p> <span class="grey">Teléfono:</span> (598) 2706-4443<br>
							</div>
						</div>
						<div class="col-sm-4 to_animate" data-animation="pullDown">
							<div class="teaser text-center">
								<div class="teaser_icon highlight size_small"> <i class="rt-icon2-location2"></i> </div>
								<p> Av. 18 de Julio 841 Of. 102<br> Montevideo, Uruguay </p>
							</div>
						</div>
						<div class="col-sm-4 to_animate" data-animation="pullDown">
							<div class="teaser text-center">
								<div class="teaser_icon highlight size_small"> <i class="rt-icon2-world"></i> </div>
								<p>info@mvdasesores.com</p>
								<p> <a href="#" class="social-icon color-icon socicon-twitter"></a> <a href="#" class="social-icon color-icon socicon-facebook"></a> <a href="#" class="social-icon color-icon socicon-google"></a> <a href="#" class="social-icon color-icon socicon-pinterest"></a>									</p>
							</div>
						</div>
					</div>
					<div class="row topmargin_60">
						<div class="col-sm-12 to_animate">
							<form class="contact-form ds parallax overlay_color with_padding big-padding columns_padding_10" method="post" action="./">
								<div class="row">
									<div class="col-sm-6">
										<p class="form-group"> <label for="name">Full Name <span class="required">*</span></label> <i class="fa fa-user highlight" aria-hidden="true"></i> <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Nombre Completo">											</p>
										<p class="form-group"> <label for="email">Email address<span class="required">*</span></label> <i class="fa fa-envelope highlight" aria-hidden="true"></i> <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email">											</p>
										<p class="form-group"> <label for="subject">Subject<span class="required">*</span></label> <i class="fa fa-phone highlight" aria-hidden="true"></i> <input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Teléfono">											</p>
									</div>
									<div class="col-sm-6">
										<p class="contact-form-message form-group"> <label for="message">Message</label> <i class="fa fa-comment highlight" aria-hidden="true"></i> <textarea aria-required="true" rows="8" cols="45" name="message" id="message" class="form-control" placeholder="Mensaje"></textarea> </p>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<p class="contact-form-submit text-center topmargin_30"> <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button min_width_button">Enviar Mensaje</button> </p>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3272.0068351187233!2d-56.2007143847622!3d-34.906277980382264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f802b65174619%3A0xa9ec1cfc46e4b7e9!2sOf.%20102%2C%20Av.%2018%20de%20Julio%20841%2C%2011100%20Montevideo%2C%20Departamento%20de%20Montevideo%2C%20Uruguay!5e0!3m2!1ses-419!2sar!4v1596811231797!5m2!1ses-419!2sar" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" style="padding: 10px;"></iframe>		 
						 
			<?php require_once("partials/footer.php"); ?>
</body>

</html>